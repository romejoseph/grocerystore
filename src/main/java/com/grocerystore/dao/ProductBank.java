package com.grocerystore.dao;

import com.grocerystore.model.Product;

public interface ProductBank {
    Product get(int id);
}
