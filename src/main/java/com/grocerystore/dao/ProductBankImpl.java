package com.grocerystore.dao;

import com.grocerystore.enums.ProductType;
import com.grocerystore.model.Product;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ProductBankImpl implements ProductBank {

    private Map<Integer, Product> products;

    public ProductBankImpl() {
        try {
            products = readProductsFromFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Product get(int id) {
        return new Product().clone(products.get(id));
    }

    private HashMap<Integer, Product> readProductsFromFile() throws FileNotFoundException {
        String productsJsonString = readProductsJsonStringFromFile();
        JSONArray productsJsonArray = new JSONArray(productsJsonString);

        return getIntegerProductHashMap(productsJsonArray);
    }

    private String readProductsJsonStringFromFile() throws FileNotFoundException {
        StringBuilder productsJsonString = new StringBuilder();
        File productsFile = new File("src/main/resources/products.json");
        Scanner scanner = new Scanner(productsFile);

        while (scanner.hasNextLine())
            productsJsonString.append(scanner.nextLine());

        return productsJsonString.toString();
    }

    private HashMap<Integer, Product> getIntegerProductHashMap(JSONArray productsJsonArray) {
        HashMap<Integer, Product> products = new HashMap<>();

        for(int i = 0; i < productsJsonArray.length(); i++) {
            JSONObject productJson = productsJsonArray.getJSONObject(i);
            Product product = getProductFromJson(productJson);

            products.put(product.getId(), product);
        }
        return products;
    }

    private Product getProductFromJson(JSONObject productJson) {
        Product product = new Product();

        product.setId(productJson.getInt("id"));
        product.setName(productJson.getString("name"));
        product.setPricePerUnit(productJson.getFloat("pricePerUnit"));
        product.setType(ProductType.valueOf(productJson.getString("type")));

        return product;
    }
}
