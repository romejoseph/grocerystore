package com.grocerystore.dao;

import com.grocerystore.model.Promotion;

import java.util.List;

public interface PromotionBank {
    Promotion getByProductId(int productId);
    List<Promotion> getAll();
    boolean hasProduct(int productId);
}