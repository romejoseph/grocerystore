package com.grocerystore.dao;

import com.grocerystore.model.Promotion;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class PromotionBankImpl implements PromotionBank {

    private Map<Integer, Promotion> promotions;

    public PromotionBankImpl() {
        try {
            promotions = readPromotionsFromFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Promotion getByProductId(int productId) {
        return promotions.get(productId);
    }

    @Override
    public List<Promotion> getAll() {
        return new ArrayList<>(promotions.values());
    }

    @Override
    public boolean hasProduct(int productId) {
        return promotions.values().stream().anyMatch(promotion -> promotion.getProductId() == productId);
    }

    private HashMap<Integer, Promotion> readPromotionsFromFile() throws FileNotFoundException {
        String promotionsJsonString = readPromotionsJsonStringFromFile();
        JSONArray promotionsJsonArray = new JSONArray(promotionsJsonString);

        return getIntegerPromotionHashMap(promotionsJsonArray);
    }

    private String readPromotionsJsonStringFromFile() throws FileNotFoundException {
        StringBuilder promotionsJsonString = new StringBuilder();
        File promotionsFile = new File("src/main/resources/promotions.json");
        Scanner scanner = new Scanner(promotionsFile);

        while (scanner.hasNextLine())
            promotionsJsonString.append(scanner.nextLine());

        return promotionsJsonString.toString();
    }

    private HashMap<Integer, Promotion> getIntegerPromotionHashMap(JSONArray promotionsJsonArray) {
        HashMap<Integer, Promotion> promotions = new HashMap<>();

        for(int i = 0; i < promotionsJsonArray.length(); i++) {
            JSONObject promotionJson = promotionsJsonArray.getJSONObject(i);
            Promotion promotion = getPromotionFromJson(promotionJson);

            promotions.put(promotion.getProductId(), promotion);
        }
        return promotions;
    }

    private Promotion getPromotionFromJson(JSONObject promotionJson) {
        Promotion promotion = new Promotion();

        promotion.setId(promotionJson.getInt("id"));
        promotion.setProductId(promotionJson.getInt("productId"));
        promotion.setBuy(promotionJson.getInt("buy"));
        promotion.setFree(promotionJson.getInt("free"));

        return promotion;
    }
}
