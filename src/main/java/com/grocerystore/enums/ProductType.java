package com.grocerystore.enums;

public enum ProductType {
    PIECE,
    BULK;
}
