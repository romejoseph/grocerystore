package com.grocerystore.model;

import com.grocerystore.enums.ProductType;

public class Product {
    private int id;
    private String name;
    private float pricePerUnit;
    private ProductType type;

    public Product() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(float pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public Product clone(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.pricePerUnit = product.getPricePerUnit();
        this.type = product.getType();

        return this;
    }
}