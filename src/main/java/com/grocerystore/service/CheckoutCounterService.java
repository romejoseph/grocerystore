package com.grocerystore.service;

import com.grocerystore.dao.ProductBank;
import com.grocerystore.dao.ProductBankImpl;
import com.grocerystore.dao.PromotionBank;
import com.grocerystore.dao.PromotionBankImpl;
import com.grocerystore.model.Product;
import com.grocerystore.model.Promotion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.concurrent.ConcurrentHashMap.newKeySet;
import static java.util.stream.Collectors.toList;

public class CheckoutCounterService {
    private List<Product> products;
    private float totalPrice;
    private ProductBank productBank;
    private PromotionBank promotionBank;

    public CheckoutCounterService() {
        productBank = new ProductBankImpl();
        promotionBank = new PromotionBankImpl();
        resetCounter();
    }

    public void resetCounter() {
        products = new ArrayList<>();
        totalPrice = 0;
    }

    public void addProduct(int productId) {
        addProducts(productId, 1);
    }

    public void addProduct(int productId, int unitCount) {
        addProducts(productId, unitCount);
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public int getItemCount() {
        return products.size();
    }

    public void printReceipt() {
        StringBuilder receiptOutput = new StringBuilder();

        printReceiptHead();

        List<Integer> soldProductsId  = products.stream().filter(distinctByKey(Product::getId)).map(Product::getId).collect(toList());
        soldProductsId.forEach(soldProductId -> {
            List<Product> soldProducts = products.stream().filter(product -> product.getId() == soldProductId).collect(toList());
            Product soldProduct = soldProducts.get(0);
            float productSales = getProductSales(soldProductId, soldProducts, soldProduct);

            receiptOutput.append("\n")
                    .append(soldProducts.size())
                    .append("\t\t")
                    .append(String.format("%-26s", soldProduct.getName()))
                    .append(String.format("%15.1f", productSales));
        });

        receiptOutput.append("\n\t\t\t\t\t\t\t\tTOTAL:\t\t");
        receiptOutput.append(totalPrice);

        System.out.print(receiptOutput.toString());

        printReceiptFoot();
    }

    private float getProductSales(Integer soldProductId, List<Product> soldProducts, Product soldProduct) {
        float productSales;

        if(promotionBank.hasProduct(soldProductId)) {
            Promotion promotion = promotionBank.getByProductId(soldProductId);

            productSales = (soldProducts.size() / (promotion.getBuy() + promotion.getFree())) * (soldProduct.getPricePerUnit() * promotion.getBuy());
            if(soldProducts.size() % (promotion.getBuy() + promotion.getFree()) >= promotion.getBuy())
                productSales += soldProduct.getPricePerUnit() * promotion.getBuy();
            else
                productSales += soldProduct.getPricePerUnit() * (soldProducts.size() % (promotion.getBuy() + promotion.getFree()));
        } else {
            productSales = soldProduct.getPricePerUnit() * soldProducts.size();
        }
        return productSales;
    }

    private void printReceiptHead() {
        String receiptHead = "\t\t\t\tGrocery Store, Inc.\n\t\t\t" +
                new Date() +
                "\n---------------------------------------------------" +
                "\nQty.\tItem name\t\t\t\t\t\t\tPrice" +
                "\n---------------------------------------------------";

        System.out.print(receiptHead);
    }

    private void printReceiptFoot() {
        String receiptHead = "\n---------------------------------------------------" +
                "\n\t\t\t\t\tThank you!\n\n"/* +
                "\n---------------------------------------------------"*/;

        System.out.print(receiptHead);
    }

    private void addProducts(int productId, int unitCount) {
        Product product = productBank.get(productId);

        if(promotionBank.hasProduct(productId))
            computeTotalPriceWithPromo(productId, unitCount, product);
        else
            computeTotalPriceWithoutPromo(unitCount, product);
    }

    private void computeTotalPriceWithPromo(int productId, int unitCount, Product product) {
        Promotion promotion = promotionBank.getByProductId(productId);

        for (int i = 0; i < unitCount; i++) {
            int productCount = getScannedProductCount(productId);

            if (productCount % (promotion.getBuy() + promotion.getFree()) < promotion.getBuy())
                totalPrice += product.getPricePerUnit();

            products.add(product);
        }
    }

    private void computeTotalPriceWithoutPromo(int unitCount, Product product) {
        for (int i = 0; i < unitCount; i++) {
            totalPrice += product.getPricePerUnit();
            products.add(product);
        }
    }

    private int getScannedProductCount(int productId) {
        return (int) products.stream().filter(p -> p.getId() == productId).count();
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
