import com.grocerystore.service.CheckoutCounterService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CheckoutCounterTest {
    private CheckoutCounterService checkoutCounterService;

    public CheckoutCounterTest() {
        checkoutCounterService = new CheckoutCounterService();
    }

    @Before
    public void resetCheckoutCounter() {
        checkoutCounterService.resetCounter();
    }

    @Test
    public void addOneProduct() {
        checkoutCounterService.addProduct(1);

        assertEquals(32, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(1, checkoutCounterService.getItemCount());
    }

    @Test
    public void addMultipleProducts() {
        checkoutCounterService.addProduct(0);
        checkoutCounterService.addProduct(1);
        checkoutCounterService.addProduct(2);

        assertEquals(218, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(3, checkoutCounterService.getItemCount());
    }

    @Test
    public void addByBulkProduct() {
        checkoutCounterService.addProduct(6, 2);

        assertEquals(292, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(2, checkoutCounterService.getItemCount());
    }

    @Test
    public void addByBulkProducts() {
        checkoutCounterService.addProduct(6, 2);
        checkoutCounterService.addProduct(7, 4);
        checkoutCounterService.addProduct(8, 3);

        assertEquals(858, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(9, checkoutCounterService.getItemCount());
    }

    @Test
    public void addByPieceAndByBulkProducts() {
        checkoutCounterService.addProduct(5);
        checkoutCounterService.addProduct(9, 2);

        assertEquals(316, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(3, checkoutCounterService.getItemCount());
    }

    @Test
    public void addPromotedProducts1() {
        checkoutCounterService.addProduct(0);
        checkoutCounterService.addProduct(1);
        checkoutCounterService.addProduct(0);
        checkoutCounterService.addProduct(2);
        checkoutCounterService.addProduct(0);

        assertNotEquals(342, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(280, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(5, checkoutCounterService.getItemCount());
    }

    @Test
    public void addPromotedProducts2() {
        checkoutCounterService.addProduct(4);
        checkoutCounterService.addProduct(1);
        checkoutCounterService.addProduct(4);
        checkoutCounterService.addProduct(4, 2);
        checkoutCounterService.addProduct(4);
        checkoutCounterService.addProduct(2);

        assertNotEquals(371, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(285, checkoutCounterService.getTotalPrice(), 0);
        assertEquals(7, checkoutCounterService.getItemCount());
    }

    @Test
    public void printReceipt() {
        checkoutCounterService.addProduct(4);
        checkoutCounterService.addProduct(1);
        checkoutCounterService.addProduct(4);
        checkoutCounterService.addProduct(4, 2);
        checkoutCounterService.addProduct(4);
        checkoutCounterService.addProduct(2);

        checkoutCounterService.printReceipt();
    }

}
